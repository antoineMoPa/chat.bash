file_server(){
    
    mime=""
        
    if [[ $request_path = "" ]]
    then
	content=`cat "index.html"`
	mime="text/html"
	content="${content}"
	# replace {{request}} by the request
	content=${content/"{{request}}"/${request}}
	content=${content/"{{request_path}}"/${request_path}}
    else
	# read the serve file and
	# verify if we should serve the
	# file at current path
	found=-1;
	while read allowed; do
	    if [[ $allowed = $request_path ]]
	    then
		# find mime type
		mime=$(find_mime_type $allowed);
		content=$(cat $allowed);
		found=1;
	    fi
	done < serve
	
	if [[ $found = -1 ]]
	then
	    send_404;
	fi
    fi
    
    # http headers
    echo "HTTP/1.1 200 OK";
    echo "Content-Type: ${mime}; charset=UTF-8";
    # this syntax "${#content} gives the chararcter count"
    echo "Content-Length: ${#content}";
    echo "Accept-Ranges: bytes";
    echo "Connection: close";
    echo "";
    # the content
    echo "$content";
}

send_404(){
    echo "HTTP/1.1 404 Not Found";
    echo "Connection: close";
    echo "";
}
