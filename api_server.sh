request_headers=""
request_sec_websocket_key=""
request_path=""

read_request_headers(){
    request_path="/"
    # read headers
    while read -r line;
    do
        # detect end of header (empty line)
        if [[ $line =~ ^[[:space:]]*$ ]]
        then
            break;
        fi
        # detect path
        if [[ $line =~ ^GET\ ([^\ $]*) ]]
        then
            request_path="${BASH_REMATCH[1]}";
        fi
	if [[ $line =~ ^Sec-WebSocket-Key:\ ([^\ $]*) ]]
	then
            wskey="${BASH_REMATCH[1]}";
	    len=$(echo -n $wskey | wc -c);
	    wskey=${wskey:0:len-1}
	    request_sec_websocket_key=$wskey
        fi
        request_headers="${request}""$line"
    done
    # Remove slash ('/') at begining of path
    len=${#request_path}
    request_path=${request_path:1:len}
}

# Serves api scripts in /api/<API-path>.sh
# Every script in the api folder is reachable
# at the given port
#
# EXAMPLE
#
# 127.0.0.1:4444/api-path
# executes
# the script at api/api-path.sh


api_server(){
    api_file="api/"$request_path".sh"
    if [[ -e $api_file ]]
    then
	. $api_file
    else
	file_server
    fi
}
