var can = document.getElementById("canvas-meow");
var ctx = can.getContext("2d");

var w = document.body.clientWidth;
var h = window.innerHeight;

can.width = w;
can.height = h;
ctx.fillRect(0,0,w,h);

var actions = [];

var objects = [];

can.onmousemove = mousemove;
can.onmousedown = mousedown;
can.onmouseup = mouseup;

current_object = -1;
new_line();


var down = false;

function new_line(){
    objects.push({
	type: "lines",
	points: []
    });
    current_object++;
}

function point_of(e){
    var x = e.clientX;
    var y = e.clientY;
    x /= w;
    y /= w;
    var p = [x,y];
    return p;
}

function mousemove(e){
    if(down){
	var p = point_of(e);
	var pts = objects[current_object].points;
	if(pts.length == 0){
	    pts.push(p);
	} else {
	    var last_point = pts[pts.length - 1];
	    if(d(p,last_point) * w > 10){
		pts.push(p);
	    }
	}
    }
}

function mousedown(){
    down = true;
}

function mouseup(){
    down = false;
    new_line();
}

init_socket();

setInterval(communicate,100);

var socket;

function init_socket(){
    var protocol = "ws:";
    var host = window.location.host;
    var url = protocol+"//"+host;
    socket = new WebSocket(url+"/drawing");
}

function communicate(){
    
}

function d(p1,p2){
    return Math.sqrt(
	Math.pow(
	    p2[1] - p1[1],2
	) + 
	    Math.pow(
		p2[0] - p1[0],2
	    )
    );
}

setInterval(draw,100);

function draw(){
    for(i in objects){
	var points = objects[i].points;
	ctx.fillStyle = "#fff";
	ctx.strokeStyle = "#fff";
	ctx.beginPath();
	for(j in points){
	    var p = points[j];
	    var x = p[0] * w;
	    var y = p[1] * w;
	    var pw = 2;
	    if(j == 0){
		ctx.moveTo(x - pw,y - pw,pw*2,pw*2);
	    } else {
		ctx.lineTo(x - pw,y - pw,pw*2,pw*2);
	    }
	}
	ctx.stroke();
    }
}
