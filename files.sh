#!/bin/bash

find_mime_type(){
    file=$1
    mime=$(file -b --mime-type $file)

    # a lot of things are detected as text/plain
    # but they are not.
    # example: text/css text/javascript, etc.
    if [[ "$mime" = "text/plain" ]]
    then
	if [[ $file =~ .css$ ]]
	then
	    mime="text/css";
	elif [[ $file =~ .js$ ]]
	then
	    mime="text/javascript";
	fi
    fi

    echo $mime
}
