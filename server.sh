#!/bin/bash

# the dot means include
# (yes, wtf)
. files.sh
. api_server.sh
. file_server.sh

if [[ $1 = "serve" ]]
then
    read_request_headers
    # serve the API
    api_server

    nc -l -p 4444 -c 'bash chat.sh serve'
else
    # start a couple of listener process
    port="4444"
    for i in seq 10
    do
	nc -l -p $port -c 'bash chat.sh serve' &
    done
    nc -l -p $port -c 'bash chat.sh serve'
fi
